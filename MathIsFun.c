/*
 * MathIsFun.c
 *
 *  Created on: 28 Februaru 2016
 *  Author: Yonatan Tzederbaum & Israel Gubi
 */

#include "MathIsFun.h"

 /* @param x - An integer for which the function applies
 * @return the value of |_sqrt(x)_|, if x is negative then the
 * function returns -1.
 * time complexity: O(logn)
 */
int funSqrt(int x);
int extract_digits(int n, char digits[]);

/*
 * Calculates the the reminder of pow(x, n) devided by d(x^n mod d)
 * funPow(5,2,20) = 5
 * funPow(-3,3,5) = 3
 * @param x - the base value
 * @param n - non-negative integer which is the exponent value
 * @param d - positive integer which is the divisor value
 * @return the value of pow(x,n) mod d.
 * time complexity: O(logn)
 */
int funPow(int x, int n, int d) {
	int rem = 0; /* the remainder of this operation -> the output */

	if (n == 0) /* every integer with power 0 equals 1, even 0^1*/
		return 1;
	else if (x == 0) /* 0 with every power equals 0 */
		return 0;
	else if (n == 1) {
		if (x < 0) { /* in C modolu for negative integers returns
		 	 negative and we need to return positive remainder */
			rem = (-x) % d; /* therefore, we calculate the modolu for the
			 	 	 	 positive number and then return the remainder */
			return d - rem;
		} else {
			rem = x % d; /* simple modolu for positive numbers */
			return rem;
		}
	} else {
		if (n % 2 == 0) { /*recursion using the fact that x^n mod
		 	 	 	 	 	d = x^(n/2) mod d * x^(n/2) mod d) */
			rem = funPow(x, n / 2, d);
			rem = (rem * rem) % d;
			return rem;
		} else {
			rem = funPow(x, (n - 1) / 2, d); /*if n is not even then we
			 	 	 	 	 	 	 	  need to cant divide n to 2 */
			rem = (rem * rem) % d;
			rem = funPow(x, 1, d) * rem;
			return (rem % d);
		}
	}
}

/*
 * Calculates the largest integer less or equal than the square root of x.
 * funSqrt(10) = 3
 * funSqrt(16) = 4
 * @param x - An integer for which the function applies
 * @return the value of |_sqrt(x)_|, if x is negative then the function
 * returns -1.
 * time complexity: O(logx)
 */
int funSqrt(int x) {
	//impelemts a binary search for the sqrt
	// search area is [low, high]
	int low = 0;
	int high = x;

	int i = x / 2; // 'i' is the current "guess"

	// x is negative return -1
	if (x < 0) {
		return -1;
	}

	/*
	 look for i s.t i^2 <= x < (i+1)^2
	 each iteration cuts the search area in two (approximately).
	 */
	while (true) {

		if (i * i > x) // 'i' is too large, set high to i-1
				{
			high = i - 1;
		} else if ((i + 1) * (i + 1) <= x) // 'i' is too small set low to i+1
				{
			low = i + 1;
		} else // found it!
		{
			return i;
		}

		i = (low + high) / 2; // set next guess to the "middle"
	}

	// cannot reach here
	return -1;
}

/*
 * Checks wheter the number given as input is a prime
 * funPrimeCheck(9) = false
 * funPrimeCheck(11) = true
 * @param x - An integer for which the function applies
 * @return boolean: true iff x is a prime
 * time complexity: O(sqrt(x)) 
 */
bool funPrimeCheck(int x) {
	int i;

	// zero, one or negative are not primes
	if (x <= 1) {
		return false;
	}

	// check wheter there's a number 2 <=i < x
	// s.t i devides x <=> x % i == 0
	// if there is its not a prime else
	// it is.
	// its enough to check numbers up untill
	// sqrt (so O(sqrt(x)), beacuse if i devides x
	// there exists j s.t i*j = x
	// and if i >= sqrt(x) then j <= sqrt(x)
	// so we will find j.
	// also we note that its ok that funsqrt rounds down
	// because if we round up and sqared we get a number
	// larger than x
	for (i = 2; i <= funSqrt(x); i++) {
		// checks if i devides x
		if (x % i == 0) {
			return false;
		}
	}

	return true;
}

/*
 * extracts digits of n into a char 
 * array, each cell will hold next digit
 * in LSB order
 * @param n - A positive integer to get digit from
 * @param - digits, array to store digits in
 * @return int: number of digits
 * time complexity: O(logx) (base 10)
 */
int extract_digits(int n, char digits[]) {
	if (n == 0) // special case
			{
		digits[0] = 0;
		return 1;
	}
	// while loop extracts digits of the number to the digit array
	// from least significant to most.
	int i = 0; //index in digit array to store current digit to
	while (true) {
		if (n == 0) {
			break;
		}

		int digit = n % 10; // current digit
		digits[i] = digit; //store digit in i'th index

		n /= 10; // devide number by 10 so we can get next
				 // digit in next iteration
		i++;     //increase index in array
	}

	return i;
}

/*
 * Checks wheter the number given as input is a palindrom
 * funPalindromeCheck(12321) = true
 * funPalindromeCheck(12322) = false
 * @param x - An integer for which the function applies
 * @return boolean: true iff x is a palindrom
 * time complexity: O(logx) (base 10)
 */
bool funPalindromeCheck(int x) {
	//unsigned number can have <+ 20 digits on 64bit machine.
	//less on 32bit. 20 digits are enough for any case.
	char digits[20] = { 0 };
	int num_digits;
	int index1;
	int index2;

	// check wheter number is negative.
	if (x < 0) {
		return false;
	}

	num_digits = extract_digits(x, digits);

	// check if digits[i] == digits[number_of_digits(x)-i]
	// for all 0 <= i <= num_digits(x)/2

	index1 = 0;              // index of i digit
	index2 = num_digits - 1; // index of num_digits  -i
	while (index1 <= index2) {
		if (digits[index1] != digits[index2]) //if different -> not a palindrom
				{
			return false;
		}

		index1++;
		index2--;
	}

	// all digits comapred were equal so this is a palindrom
	return true;
}
