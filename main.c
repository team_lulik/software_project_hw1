/*
 * main.c
 *
 *  Created on: 20 ���� 2016
 *      Author: USER
 */
#include <stdio.h>
#include <stdbool.h>
#include "MathIsFun.h"

int main(void){
	int OpNum,x=0,n=0,d=0,val=0; /* val is the output for operation1 */
	bool val2; /* gets the output of operation 2 or 3 */

	printf("Welcome to Math Is Fun - beta version\n");
	printf("Supported operations are:\n");
	printf("1 - Power Calculation\n");
	printf("2 - Prime Check\n");
	printf("3 - Palindrome Check\n");
	printf("Please enter operation number (1/2/3): \n");

	scanf("%d",&OpNum); /*gets the operation number from the user */

	if (OpNum == 1){
		printf("Please enter three space separated numbers: \n");
		scanf("%d %d %d",&x,&n,&d); /* gets the numbers for funpow from the user */
		val = funPow(x,n,d);
		printf("res = %d\n",val); /* returns what is the residue */
	}
	else if (OpNum == 2 || OpNum == 3){
		printf("Please enter an integer: \n");
		scanf("%d",&x); /* get the number for operations 2 or 3 */
		if (OpNum == 2){
			val2 = funPrimeCheck(x);
		}
		else{
			val2 = funPalindromeCheck(x);
		}
		if (val2 == true){
			printf("res = true\n");
		}
		else{
			printf("res = false\n");
		}
	}
	return 0;
}

